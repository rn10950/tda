<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

/*
Provides:
$theme - integer of theme CSS file
$themeCSS - string of site root relative path to selected CSS file
$useArchiveLinks - bool

*/
$themeRoot = "/themes/";
// theme
if(isset($_COOKIE["tda-theme"])) {
	$cook = $_COOKIE['tda-theme'];
	if(is_numeric($cook)) {
		$theme = $cook;
	} else {
		// invalid cookie, return default theme
		$theme = 0;
	}
	if($cook == 9) {
		// time machine mode
		$autoTheme = true;
		if(isset($thDate)) {
			switch (true) {
				// before 1/1/16
				case ($thDate < 1451624400):
					$theme = 8;
				break;
				// 2/20/16
				case ($thDate < 1455944400):
					$theme = 7;
				break;
				// 7/15/16
				case ($thDate < 1468594200):
					$theme = 6;
				break;
				// immediately post election
				case ($thDate < 1478676900):
					$theme = 5;
				break;
				// 11/24/16
				case ($thDate < 1479963600):
					$theme = 4;
				break;
				// 1/20/2017
				case ($thDate < 1484931600):
					$theme = 3;
				break;
				// 5/20/2017
				case ($thDate < 1495252800):
					$theme = 2;
				break;
				// 8/1/2017
				case ($thDate < 1501560000):
					$theme = 1;
				break;
				default:
					$theme = 0;
			}
		} else {
			$theme = 0;
			echo "<!-- theme date not set -->";
		}
		//$theme = 8;
	}
} else {
	$theme = 0;
}

$themeCSS = $themeRoot . $theme . ".css";

// archive links
if(isset($_COOKIE["tda-archive"])) {
	$cook = $_COOKIE['tda-archive'];
	if($cook == "on") {
		$useArchiveLinks = true;
	} else {
		$useArchiveLinks = false;
	}	
} else {
	$useArchiveLinks = false;
}

?>