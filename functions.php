<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

// includes
require_once 'php-markdown/Michelf/MarkdownExtra.inc.php';
require_once 'linkify.php';
require_once 'urlproc.php';
require_once 'vars.php';


// set time zone to eastern (trump time)
date_default_timezone_set('America/New_York');

// windows/linux setting
function isWindows() {
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		return true;
	} else {
		return false;
	}
}

// client ip addr
function getIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])){
		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
	}
	//whether ip is from proxy
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	//whether ip is from remote address
	else {
		$ip_address = $_SERVER['REMOTE_ADDR'];
	}
	return $ip_address;
}

// youtube embed
function ytEmbed($id) {
	return '<iframe class="media-embed" width="560" height="315" src="https://www.youtube.com/embed/' . $id . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
}

// markdown parser
function mdhtml($md) {
	// put this in function as it is anticipated that additional
	// work will need to be done on this
	$rawHtml = Michelf\MarkdownExtra::defaultTransform($md);
	return linkify($rawHtml, array("http", "https", "ftp", "mail"), array(""));
}

// error print function
function printErr($e) {
	echo '<hr>' . $e . '<hr>';
}

// array printer
function print_a($a) {
	echo '<pre>';
	print_r($a);
	echo '</pre>';
}

// if attribute present and value check
function recursiveFind(array $haystack, $needle, $val)
{
    $iterator  = new RecursiveArrayIterator($haystack);
    $recursive = new RecursiveIteratorIterator(
        $iterator,
        RecursiveIteratorIterator::SELF_FIRST
    );
    foreach ($recursive as $key => $value) {
        if ($key == $needle) {
            //yield $value;
			if($value == $val) {
				return true;
			}
        }
    }
	return false;
}

// Function to check if the string ends  
// with given substring or not 
function endsWith($string, $endString) { 
    $len = strlen($endString); 
    if ($len == 0) { 
        return true; 
    } 
    return (substr($string, -$len) === $endString); 
} 

//url query fn (thanks to stack overflow user ihorsl)
//usage: modify_url_query($url, array('limit' => 50));
function modify_url_query($url, $mod){
	echo "<!--";
	$purl = parse_url($url);
	$params = array();
	if (($query_str=$purl['query'])){
		parse_str($query_str, $params);
		foreach($params as $name => $value){
			if (isset($mod[$name])){
				$params[$name] = $mod[$name];
				unset($mod[$name]);
			}
		}
	}        
	$params = array_merge($params, $mod);
	$ret = "";
	if ($purl['scheme']){
		$ret = $purl['scheme'] . "://";
	}    

	if ($purl['host']){
		$ret .= $purl['host'];
	}    

	if ($purl['path']){
		$ret .= $purl['path'];
	}    

	if ($params){
		$ret .= '?' . http_build_query($params);
	}
	if ($purl['fragment']){
		$ret .= "#" . $purl['fragment'];
	}      
	echo "-->";
	return $ret;
}

//get text between (thanks to JustinCook.com)
//usage: get_string_between($string, $strBefore, $strAfter);
function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

// settings loader (should remain at end of file)
require "settingsLoader.php";

?>