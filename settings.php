<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019, 2020 rn10950

require_once "functions.php";

$themesArr = [
		"Default Theme",
		"Theme 1",
		"Theme 2",
		"Theme 3",
		"Theme 4",
		"Theme 5",
		"Theme 6",
		"Theme 7",
		"Theme 8",
		"Auto",
	];

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Settings - The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="/tda.css">
		<script src="jquery.min.js"></script>
		<script src="siema.min.js"></script>
		<script src="settings.js"></script>
	</head>
	<body>
		<?php require 'header.php'; ?>
		<span style="display:none" id="tda-settings-theme"><?php echo $theme; ?></span>
		<div class="tda-body">
				<h1>Settings</h1>
				<p>
					Settings are saved in a browser cookie and will only be saved in this browser.
				</p><br>
				<form action="saveSettings.php" method="post" class="tda-body">
					<label for="theme" class="tda-settings-option">Theme: </label>
					<select name="theme" id="sel-theme">
						<?php
						if(isset($autoTheme)) {
							$theme = 9;
						}
						foreach($themesArr as $idx=>$name) {
							echo '<option value="' . $idx . '" ' . (($theme == $idx) ? "selected" : "") . '>' . $name . '</option>';
						}
						?>
					</select><br>
					<h3>Theme Gallery:</h3>
					<div class="siema">
						<img src="/themes/preview/0.png" class="theme-slide-image">
						<img src="/themes/preview/1.png" class="theme-slide-image">
						<img src="/themes/preview/2.png" class="theme-slide-image">
						<img src="/themes/preview/3.png" class="theme-slide-image">
						<img src="/themes/preview/4.png" class="theme-slide-image">
						<img src="/themes/preview/5.png" class="theme-slide-image">
						<img src="/themes/preview/6.png" class="theme-slide-image">
						<img src="/themes/preview/7.png" class="theme-slide-image">
						<img src="/themes/preview/8.png" class="theme-slide-image">
					</div>
					<button class="prev" type="button">Prev</button>
					<button class="next" type="button">Next</button>
					<span id="theme-slide-caption">Caption</span><br>
					<hr class="settings-hr">
					<label for="archive" class="tda-settings-option">Use Archive Links Mode: </label>
					<input type="checkbox" name="archive" <?php if($useArchiveLinks){echo 'checked'; }?>>
					<p>
						Redirect all outgoing links through archive.is. This not only prevents you 
						from giving advertising revenue to fake news sites, but also helps archive many 
						of the pages in this collection that have not already been archived. If the page
						has already been archived, you will be taken to the archived copy. Please do not
						re-archive a page if it already has been archived.
					</p>
					
					
					<br><br><input type="submit" value="Save">
				</form>
		</div>
	</body>
</html>