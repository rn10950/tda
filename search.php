<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019, 2020 rn10950

require_once "functions.php";

$hSearch = true;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Search - The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="/tda.css">
	</head>
	<body>
		<?php require 'header.php'; ?>
		<div class="tda-body">
			<h1>The_Donald Archive Web Frontend Search</h1>
			<form action="results.php" method="get" class="tda-body">
				<label>Search Term: </label><input type="text" name="q"><br>
				<h4>Date (M/D/YYYY):</h4>
				<input type="checkbox" name="chkStartDate"><label>Start Date: </label><input type="text" name="startDate"><br>
				<input type="checkbox" name="chkEndDate"><label>End Date: </label><input type="text" name="endDate"><br>
				<h4>Score:</h4>
				<input type="checkbox" name="chkMinScore"><label>Minimum Score: </label><input type="text" name="minScore"><br>
				<input type="checkbox" name="chkMaxScore"><label>Maximum Score: </label><input type="text" name="maxScore"><br>
				<h4>Comment Count:</h4>
				<input type="checkbox" name="chkMinCC"><label>Minimum Comment Count: </label><input type="text" name="minCC"><br>
				<input type="checkbox" name="chkMaxCC"><label>Maximum Comment Count: </label><input type="text" name="maxCC"><br>
				<h4>Domain:</h4>
				<input type="checkbox" name="chkDomain"><label>Domain: </label><input type="text" name="domain"><br>
				<h4>Sort:</h4>
				<table>
					<tr>
						<td class="tda-body">
							<input type="radio" name="sort" value="score" checked><label>Score</label><br>
							<input type="radio" name="sort" value="title"><label>Title</label><br>
							<input type="radio" name="sort" value="created_utc"><label>Date</label><br>
						</td>
						<td class="tda-body">
							<input type="radio" name="sortDir" value="ASC" checked><label>Ascending</label><br>
							<input type="radio" name="sortDir" value="DESC"><label>Descending</label><br>
						</td>
					</tr>
				</table>
				<h4>Post Type:</h4>
				<input type="radio" name="type" value="all" checked><label>All</label>
				<input type="radio" name="type" value="link"><label>Link</label>
				<input type="radio" name="type" value="self"><label>Self</label>
				<br><br>
				<input type="checkbox" name="chkTextPost"><label>Search within text posts</label>
				<h4>SQL Query Mode:</h4>
				<input type="checkbox" name="chkQuery"><label>Query: </label><input type="text" name="query"><br>
				<br><br><input type="submit">
			</form>
		</div>
	</body>
</html>