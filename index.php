<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019, 2020 rn10950

require_once "functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="/tda.css">
	</head>
	<body>
		<?php require 'header.php'; ?>
		<div class="tda-body">
			<h1>The_Donald Archive</h1>
			<p>
				Welcome to the The_Donald Archive web interface. This is a
				near-complete collection of posts, comments, and images posted
				on reddit's r/The_Donald subreddit during the 5 year span of
				2015-2020. 
			</p>
			<h4>Features:</h4>
			<ul class="tda-body">
				<li>
					<b><u>Time Machine:</u></b> View r/The_Donald as it appeared
					on any specific date and time. All times eastern (TT).
				</li>
				<li>
					<b><u>Top:</u></b> View the top posts of all time
				</li>
				<li>
					<b><u>Wiki:</u></b> The_Donald's wiki as it appeared in June 2019
				</li>
				<li>
					<b><u>Advanced Search:</u></b> Search and sort the archive with advanced criteria.
				</li>
			</ul>
			<h4>Settings:</h4>
			<p>
				You can change settings on the settings page, located in the top navigation bar.
			</p>
			<ul class="tda-body">
				<li>
					<b><u>Theme:</u></b> Select your favorite theme from The_Donald's history, or choose
					Auto, which automatically selects a theme using the date the post you're viewing was created.
				</li>
				<li>
					<b><u>Archive Links Mode:</u></b> Redirect all outgoing links through archive.is.
					This not only prevents you from giving advertising revenue to fake news sites, but
					also helps archive many of the pages in this collection that have not already been archived.
				</li>
			</ul>
			<p class="tda-version">
				The_Donald Archive web interface, version <?php echo $tdaVersion; ?>. All content on this site is presented as originally
				created and does not necessarily reflect the opinions of the creators of The_Donald Archive. The_Donald
				Archive is presented for educational and historical purposes only.
			</p>
		</div>
	</body>
</html>