
<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950
//
// must be included in page, $thread_id must be set to the thread ID
//
// to create a comments section within a page call:
//     formatComments($comments, 0);
//
// provides $link, which is a submissions table row for the link

require_once "functions.php";
require_once "krumo/class.krumo.php";


$db = new SQLite3($reddDatabase);


//$thread = "t3_" . $_GET['id'];
//$thread = $_GET['id'];
$thread = $thread_id;

// submission data 
$q = 'SELECT * FROM submissions WHERE "id" = "' . $thread . '"';
$results = $db->query($q);
$link = $results->fetchArray();

// top level comments
$q = 'SELECT * FROM comments WHERE "link_id" = "t3_' . SQLite3::escapeString($thread) . '" AND "parent_id" = "t3_' . SQLite3::escapeString($thread) . '" ORDER BY score DESC';
$results = $db->query($q);
$comments = [];

while($row = $results->fetchArray()) {
	// array format: 0-data, 1-id, >=2-child arrays
	$a = array($row, $row['id']);
	array_push($comments, $a);
}
unset($results);


// recursive comment function
function populateComments($a, $db) {
	$i = 1;
	$q = 'SELECT * FROM comments WHERE "parent_id" = "t1_' . SQLite3::escapeString($a[1]) . '" ORDER BY score DESC';
	$results = $db->query($q);
	if($results == false) {
		printErr($db->lastErrorMsg());
	} else {
		while($row = $results->fetchArray()) {
			// array format: 0-data, 1-id, >=2-child arrays
			$b = array($row, $row['id']);
			array_push($a, $b);
		}
	}
	$j = 0;
	foreach($a as $c) {
		if($j <= 1) {
			$j++;
			continue;
		}
		//array_push($a, populateComments($c, $db));
		$a[$j] = populateComments($c, $db);
		$j++;
	}
	return $a;
}


$i = 0;
foreach($comments as $a) {
	$comments[$i] = populateComments($a, $db);
	$i++;
}

function formatComments($a, $j) {
	foreach($a as $b) {
		/*echo '<div class="com com-level' . $j . '">';
		echo '<span class="com-author">' . $b[0]['author'] . '</span>';
		echo '<span class="com-points">' . $b[0]['score'] . ' points</span>';
		echo '<span class="com-time">' . date("M j, Y g:i A", $b[0]['created_utc']) . '</span>';
		echo '<p class="com-body">' . mdhtml($b[0]['body']) . '</p>';
		echo '</div>';*/
		?>



<div class="noncollapsed comment">		
	<div class="midcol unvoted">
		<div class="arrow up" tabindex="0"></div>
		<div class="arrow down"></div>
	</div>


	<div class="entry unvoted">
		<p class="tagline">
			<a class="expand link-cursor" onclick="">[–]</a>
			<a href="https://old.reddit.com/user/<?php echo $b[0]['author']; ?>" class="author may-blank"><?php echo $b[0]['author']; ?></a>
			<?php
			if(isset($b[0]['author_flair_text'])) {
				echo '<span class="flair flair-' . $b[0]['author_flair_css_class'] . '" title="' . $b[0]['author_flair_text'] . '">' . $b[0]['author_flair_text'] . '</span>';
			}
			?>
			<span class="userattrs"></span>
			<span class="score unvoted" title="<?php echo $b[0]['score']; ?>"><?php echo $b[0]['score']; 
																					if($b[0]['score'] == 1) {
																						echo " point";
																					} else {
																						echo " points";
																					}
																			?></span> 
			<?php echo date("M j, Y g:i A", $b[0]['created_utc']); ?>
			
			<!--<span class="awardings-bar" data-subredditpath="/r/AskReddit/"></span>&nbsp;<a href="javascript:void(0)" class="numchildren" onclick="return togglecomment(this)">(12 children)</a>-->
			
		</p>
		

		<div class="usertext-body may-blank-within md-container ">
			<div class="md">
				<?php echo mdhtml($b[0]['body']); ?>
			</div>
		</div>
		
		
		
		<ul class="flat-list buttons">
			<li class="first"><a href="/comments.php?commentid=<?php echo $b[0]['id']; ?>" class="bylink" rel="nofollow">permalink</a></li>
			<li><a href="#" class="embed-comment">embed</a></li>
			<li class="comment-save-button save-button"><a href="#">save</a></li>
			<li class="report-button"><a href="#" class="reportbtn">report</a></li>
		</ul>
	</div>
	<div class="child">
		<?php formatComments(array_slice($b, 2), $j + 1); ?>
	</div>
</div>



	<?php
	//formatComments(array_slice($b, 2), $j + 1);
	}
}
?>