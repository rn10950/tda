<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

// set theme cookie
if(isset($_POST['theme']) && is_numeric($_POST['theme'])) {
	setcookie("tda-theme", $_POST['theme'], 2147483647);
}

// set archive cookie
if(isset($_POST['archive'])) {
		setcookie("tda-archive", $_POST['archive'], 2147483647);
} else {
	setcookie("tda-archive", "off", 2147483647);
}

//redirect when done
header("Location: /");
?>