<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "../functions.php";
require_once "../krumo/class.krumo.php";

$hWiki = true;
$wikiContent = "index.md";

// process markdown
$wikiMd = file_get_contents($wikiContent);
$wikiHtml = mdHtml($wikiMd);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Index - The_Donald Wiki - The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="/tda.css">
		<style>
			
			.TDAhidden {
				display: none !important;
			}
			.link-cursor {
				cursor: pointer;
			}
			.siteTable {
				display: none !important;
			}
			.archive-warning {
				background-color: LightCoral;
				color: DarkRed;
				font-size: 11pt;
				padding: 10px;
			}
			.archive-warning > strong {
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<?php require '../header.php'; ?></div>
		<p class="archive-warning">
			<strong>WARNING: POTENTIALLY OUTDATED CONTENT</strong><br>
			This is a snapshot of The_Donald's wiki as of June 1st 2019. 
			Some links have been changed to reflect archived copies, but the text content is as it was on that date.
		</p>
		<h1 class="wikititle">index</h1>
		<div class="wiki-page-content md-container">
			<div class="md wiki">
				<?php 
					echo $wikiHtml;
				?>
			</div>
		</div>
		</div></div>
	</body>
</html>