 
#Why Should I consider Voting for Donald Trump?

Here at /r/The_Donald we are often asked why we support Trump by undecided voters, those who have never voted, and even disaffected Democrats who are facing the likelihood of Hillary Clinton. In an attempt to limit these questions and have information in one location, a number of the higher effort responses will be listed out here. /r/AskTrumpSupporters is another good resource for interested individuals.  

##/u/TrumpGal

[The compilation of Question Posts](/comments.php?id=447y5a)  

>I was a democrat, still am I guess other than my support of Trump. I'm not feeling particularly coherent tonight, so here are some quick thoughts.  

>I was horrified at the direction in which Bush took the country, destroying the prosperity of the Clinton era and getting us into needless wars. I was excited about Obama. I voted for him twice. Eventually I realized Obama was as big of a fraud as anyone else. TPP really solidified it for me.  

>When I really started to think about it, Obama was no different than Bush. He's not progressive at all. Sure, he came around to gay marriage (after initially being against it), he doesn't like guns (but could never do anything about it), but those are just wedge issues used to pit the parties against each other while the donor class laughs all the way to the bank. Obama and Bush are the same because they get money from the same people. When it comes to the stuff they care about, we will get the same no matter if the president is GWB, Obama, Bush, or Clinton.  

>Obama has supported the military industrial complex keeping us involved in endless wars, orchestrating regime changes and causing further destabilization in the Middle East, and arming the "moderate rebels" who are in fact al-quaeda affiliates. He's continued the Patriot Act and surveillance. He uses executive orders frequently even though he campaigned against that. Obamacare was originally developed by the conservative Heritage foundation and implemented by Romney in Massachusetts. It's a giveaway to the insurance lobby and it penalizes citizens for not buying a product on the private market - it should have been ruled unconstitutional, IMO. The TPP was drafted in secret by and for global corporations. How can the hope and change candidate support this stuff? I don't have links to support each and every point, but take a look at this list http://www.humblelibertarian.com/2011/08/bush-20-100-ways-barack-obama-is-just.html  

>I realized that we could not have another 8 years of this with Hillary Clinton. She will govern exactly like Obama, get involved in needless conflicts and sell out to corporations and big banks wherever she can. This interview with Elizabeth Warren really drives home who Hillary is: http://www.huffingtonpost.com/entry/elizabeth-warren-hillary-clinton_us_562e4eefe4b0ec0a389519e3 I also read up on how Wall Street feels basically fine with either Hillary or Jeb as president http://www.politico.com/story/2014/04/wall-street-republicans-hillary-clinton-2016-106070
Now, sorry, that was mostly about why I'm not voting for the democrats this time around even though I was at some point what you would call pretty liberal.  

>Money in politics really is the biggest issue though. No matter what a candidate says they will do, the bottom line is they are bought and paid for and they will succumb to those interests. Trump is self-funded and bypasses this entirely. This is so, so important. It means he can make appointments to people who are actually QUALIFIED. He doesn't have to give jobs to political hacks and donors. He isn't part of a machine. I don't expect him to know everything, just to hire competent people and manage them well.  

>Trump is used to running a business where HIS money is the line. Look how he runs his campaign where HIS money is on the line. It was like pulling teeth to get him to put his first 200k on radio ads, while others have dumped 10s of millions down the drain to get worse results. I want him to run the country like this, as if his own money were on the line.  

>As far as social issues, I don't think he cares either way. He said he would look at Planned Parenthood and be open to funding the non-abortion things they do. He thinks gay marriage should have been left to the states, but he disagreed with Kim Davis and said the law is now the law.  

>He's non-interventionist. He wants to bomb the shit out of ISIS, working with Russia, but he doesn't want us to interfere with sovereign nations generally. I agree with this - ISIS is like the Nazis, and they are a threat to us, we have to take them out. Other candidates want to make a no-fly zone which basically means shooting down Russian planes and going to war with Russia... for what?!  

>He wants to build our infrastructure, not spend the money on wars or "nation building." He's said it's ridiculous we put money into building schools in Iraq, which get blown up anyway, but we can't put up a school in Brooklyn?
Bringing blue collar jobs back is a big issue too. There are so many towns and counties that have become ghost towns when "the plant" closes down and moves to Mexico or China. The areas become severely economically depressed. These are people that want to work, not be on welfare. The number of people on food stamps exploded under Obama and the unemployment for blacks is depression-era. Wages overall have been stagnant for 10 years. These are the kinds of things Trump talks about.  

>In the same vein, he wants to improve our trade deals. We're a debtor nation, and it makes no sense for us to trade on terms that are so weak for us. Who benefits? Global corporations who can maximize profits by accessing more global markets but setting up shop in the countries that allow slave labor and headquartering themselves in the country with the lower taxes.  

>When it comes to immigration and security, this shouldn't even be an issue on party lines. I'm going to vote for the candidate that I think will make the country safe. Trump wants to bring back surveillance programs that worked in NYC. The Muslim ban may be extreme, but non-citizens have no constitutional right to come here. As for the wall, Hillary voted for a fence on the Mexican border that was never built, and this was the DNC platform in 1996 on immigration: https://reason.com/blog/2015/08/26/when-the-entire-democratic-party-was-lik Trump doesn't use PC language but there is nothing wrong with the concept of securing the border and deporting each and every person who is here illegally. Not all are bad people, but they are depressing wages in addition to causing crime.

>Trump has also vowed to keep Social Security and Medicare intact, and wants to repeal Obamacare. He's supported single payer and universal healthcare in the past.  

>I'm sure there's more I haven't thought of but those are some of the main points why I like him so much.  

##/u/gabillions  

Original post: [I am a former NSA/CIA guy and business owner. Here's why I'm voting for trump](/comments.php?id=48u5mz)  

>I'm absolutely tired of Trump supporters being characterized as "White, poor, and uneducated". I'm not any of those three things. It's no wonder that the country's political establishment has no idea why someone like me would vote for Trump. Here's why:  

>**My reasons for Trump:**  

>- **National security**: This is a big deal to me. The other candidates haven't put forth a single item that improves it, except Trump. People laugh when Trump mentions The Wall, but it's actually a good idea. As someone who has worked closely with CBP/DHS and who worked at the National Counter-Terrorism Center in order to prevent terrorists from entering the country, the Mexican border is one of the weakest vectors of infiltration we have. Shoring this area up has only positive benefits: putting a large stop to illegal drug/weapons/human trafficking; making it exceedingly difficult for terrorists to enter the country unnoticed; giving us the ability to refocus a lot of the money spent on personnel patrolling the border areas.  

>- **Healthcare Plan**: The plan that he just released is the absolutely best healthcare plan that any candidate has put forth. Although I was a Trump supporter beforehand, reading it made me only want to vote for Trump more, as it shows that he is both standing up for middle class americans (lowering premiums by allowing more competition; decreasing pharmaceutical costs; allowing individuals to fully deduct premiums) while still being compassionate to the poor (making sure people who can't afford healthcare can still get the healthcare they need). He even made it a point to say during the debates that he doesn't want people dying in the streets because they can't afford healthcare. Points like this are constantly glossed over as others attempt to demonize him.  

>- **Trump can make deals**: This sounds ridiculous to people who support candidates like Ted Cruz (who would take a strong stance on things), but the reality in D.C. is that you have to be willing to work with people in order to pass legislation. I absolutely recognize this need for cooperation between political parties, and I don't want a president who people aren't willing to work with (Bernie Sanders included). When you are too far left or too far right, you take an untenable position. As Trump moves to the center for the presidential election (as all candidates inevitably do), I think he will leave the most room for cooperation with other political interests.  

>- **Foreign policy**: This is hailed as Trump's weakest point, and people are not wrong. Trump as a figurehead of the United States, however, could prove to be a huge asset in one very important area -- repairing the relationship with Russia. Can you imagine a level of cooperation with a country like Russia where you're not each trying to fund multiple groups in middle-eastern proxy wars? Russia is very committed to eliminating terrorists/extremists, and cooperation with them could bring an end to the foothold that groups like ISIS have found in recent years. Strong, authoritarian figures only respect other strong, authoritarian figures, and Trump would represent the United States no longer allowing people to take advantage of its resources because of perceived weakness.  

>**Why not Bernie**  

>I get the excitement people feel over Bernie, and I started out for Bernie. The more I thought on all the things that sound nice such as free college, a living wage, racial justice, etc. the more I realized the inherent flaws with those things.  

>- **Free College**: It sounds good from the outset, as education has been a key differentiator for people to move up in income. But making college free is effectively extending the education system by 4-5 years, and removing people from the legitimate pipelines of other career paths: the military, trade schools, immediately entering the job force after high school, and so on. There is absolutely good money to be made installing HVAC systems or fixing toilets/washers/dryers, but why would someone start down that path if they feel their degree means they deserve cushy job as a middle-management type. The end result is: everyone has a degree, therefore no one has a degree. People in my industry (software development) actually tend to completely disregard someone's degree when looking at a candidate, which is more akin to what the future of the work force looks like (excepting jobs that require a degree such as an engineer, medical professional, etc.).  

>- **Living Wage**: I think most educated people can see the flaw to this type of plan. If I pay someone $15/hr plus the federally mandated medical benefits, etc. then as a business owner I would have to greatly increase the prices of my goods/services in order to have a chance at growing the business. So all the businesses increase their prices, the $15/hr buys less, homeowners/landowners increase rent in order to pay for the price increase of things, and you've effectively done nothing.   

>- **Racial Justice**: Talking about race is just about the most un-American thing you can do. Who gives a shit where your parents/grandparents came from -- Who are you going to be? The politics of the left are to divide people by race, religion, sexual orientation, and then pander to each division. Trump might be a polarizing force, but that's a very good thing. Unity in the identity of being an "American" is a much more powerful ideal than saying "We're Black/White/Asian/Hispanic and we're American", because where does the division end? Do we then split up Hispanics by the individual country? I know for a fact that El Salvadoreans see themselves as completely different than Guatemalans. Should we then make sure that workplaces are made up of 1.2% El Salvadoreans and 0.6% Guatemalans. What if you have someone born of an El Salvadorean and a Guatemalan parent. There is no logical end to division. The laws of the country should be color blind. Unity in an American identity is the answer.   

>- **National Security**: Say what you want about Obama, but when he got into office after having made so many promises related to reigning in national security elements, he was handed intelligence reports of how things really are. He then made the choice to change his stance in light of new information. Guys like Bernie are passionate, and that passion makes them stubborn. If Bernie gets into office, he would absolutely start cutting national security programs regardless of what information came in, as he has promised in his campaign platform. Knowing what I know, it would be an absolute disaster to see the country go down a path of dismantling the programs that have nullified most attempts to attack the United States.  

>**Why not Hillary**  

>- **Classified Emails**: If I had done what Hillary had done when I held a TS/SCI clearance, I would be in jail for 10+ years without hesitation from any judge presiding over the case. This is a big deal, as the definition for Top Secret (which we all had to learn by heart in the military) was "which reasonably could be expected to cause exceptionally grave damage to the national security" if disclosed. She instructed people to wipe classification headings from documents and send them to an insecure server.  

>- **Making Deals**: Hillary is the only other candidate I believe would actually be able to pass legislation, as she is also able to effectively make deals. With the way the Clintons act, though, I could never trust that their intentions would be for the good of the United States, with actions like pardoning one of the FBI's most wanted in exchange for a 1 million+ donation. In my mind, that only leaves Trump.  

>- **Other Things**: There's a laundry list of my anti-Hillary sentiment, but they're all Trumped by the classified emails scandal. She's not a viable candidate because she committed a crime and will soon receive her punishment. If I know the administration that exists though, the plan is this: 1) Push forward any criminal action as fast as possible. 2) Conclude the case before the next president takes office. 3) Pardon Hillary so that the next president (Trump) can't force the DOJ to pursue a criminal indictment that she can't be pardoned out of.  


##/u/NYPD-32  

>>When and how does Trump intend to court black voters? How will his base react if he does so?  

>I think he will have a lot to offer black voters. I've always said that the main issues of the day are "free trade" and immigration. It's been hurting everyone.  

>The immigration implications are quite large for the African American community. A lot of hispanic immigrants are moving into black areas and preying on them as part of gang warfare. Compton, possibly the most iconic black community, is now majority hispanic. I don't know where you all are from so you may be immune to it but blacks [are not well treated](http://articles.latimes.com/2013/jan/25/local/la-me-0126-compton-20130126) by many hispanics who are moving into these areas.  

>Trump has highlighted the case of Jamiel Shaw, a really good kid who was gunned down by gang violence. He was 17 and probably going to Stanford to play football. [But ...](http://homicide.latimes.com/post/jamiel-shaw/)  

>>Jamiel Shaw Jr., 17, a black youth, was shot in the head and back at 2150 5th Ave. in Arlington Heights about 8:40 p.m. on Sunday, March 2.  

>>According to police, Jamiel was walking home when two Latino men jumped out of a white car and approached him. He was asked what gang he belonged to. When he failed to respond quickly enough, they shot him, police said.  

>>The men sped away in the white car north on 5th Avenue, according to Officer Kate Lopez of LAPD's media relations office. Jamiel's father heard the shots and ran outside. He stayed by his son until medical personnel arrived, Lopez said.  

>The Southern Povery Law Center has [noted this trend](https://www.splcenter.org/fighting-hate/intelligence-report/2008/latino-vs-black-violence-drives-hate-crimes), writing a few years ago:  

>>Driven by racial violence between blacks and Latinos, hate crimes in Los Angeles rose by 28% in 2007, hitting a five-year high, according to a report issued in July by the Los Angeles County Human Relations Commission.  

>>This sharp rise in racially motivated criminal activity contrasted dramatically with the overall decline in the Los Angeles crime rate last year.  

>>The report documented 763 reported hate crimes, dominated by assault and vandalism. The largest category of crimes involved Latino suspects targeting black victims ..  

>The economic aspect has been devastating for the black community as well. This has a number of facets which include: immigrant from central/south America, foreign visa workers, and outsourcing.  

>Buzzfeed ran a [really good article](http://www.buzzfeed.com/jessicagarrison/all-you-americans-are-fired#.mrngOjp15) about this that I recommend everyone read.  

>>“All you black American people, fuck you all…just go to the office and pick up your check,” the supervisor at Hamilton Growers told workers during a mass layoff in June 2009.  

>>The following season, according to a lawsuit filed by the Equal Employment Opportunity Commission, about 80 workers, many of them black, were simply told: “All you Americans are fired.”  

>>Year after year, Hamilton Growers, which has supplied squash, cucumbers, and other produce to Wal-Mart and the Green Giant brand, hired scores of Americans, only to cast off many of them within weeks, according to the U.S. government. And time after time, the grower filled the jobs with foreign guest workers instead.  

>This is some pretty brutal stuff. You can see [here](https://research.stlouisfed.org/fred2/graph/?g=V0F) that, while it's decreasing, black unemployment was higher than average during the recession and is still hovering around the 10% range. It's not just the black community either .. white blue collar (generally considered those without college degrees) workers are facing equally high despair, and [have suicide rates](http://inthesetimes.com/article/18638/barbara-ehrenreich-americas-blue-collar-white-people-are-dying-at-astoundin) even higher than African Americans:  

>>The white working class, which usually inspires liberal concern only for its paradoxical, Republican-leaning voting habits, has recently become newsworthy for something else: according to economist Anne Case and Angus Deaton, the winner of the latest Nobel Prize in economics, its members in the 45- to 54-year-old age group are dying at an immoderate rate. While the lifespan of affluent whites continues to lengthen, the lifespan of poor whites has been shrinking. As a result, in just the last four years, the gap between poor white men and wealthier ones has widened by up to four years.  

>These problems are effecting all communities across the board. From [Disney forcing workers to train their foreign replacements](http://www.nytimes.com/2015/06/04/us/last-task-after-layoff-at-disney-train-foreign-replacements.html) to jobs being outsources to Asia leaving dying communities in the US, the problems are very real. This is a major reason for the increasing inequality gap. The rich donor class always supports mass immigration, outsourcing, and more H1B visas. Cheap labor is all that matters to them, fuck everyone else. While Bernie also touches on some of this I only trust Trump to handle them. I believe he will enact more protectionist policies and encourage support for American workers. If you have any questions feel free to PM me further or ask anyone at /r/the_donald.  

##/u/LordDwia  

>1. He can't be bought. He has no secret loyalties. He is is own man.  

>2. He's not afraid to change his mind or evolve positions. He's not afraid of what people think of him.  

>3. He's strong and won't back down when threatened. He'll hold firm to what's best for America.  

>4. He believes in hard work, and walks the talk. He's willing to give anyone a chance if they are willing to put the effort in.  

>5. He genuinely loves this country and wants it to be great. He doesn't (like many liberals and internationalists) secretly believe that America is evil or needs to be changed into something else: He believes in the original American idea.  

>6. He is the only candidate we've had in over a generation who says it like it is, speaks his mind, and isn't afraid to be wrong, offend people, or break a few eggs. He's the new Teddy Roosevelt.  

>7. I genuinely believe he will be the most honest and well-loved president we've had since the second world war. He's not for the bankers and the industrialists, he's for you and me. He'll be amazing.  

##/u/chepamec  

>You didn't really explain what you meant by "who the media makes him out to be", so I'll refute a few common misconceptions:

>>Trump is racist  

>No. He's not politically correct and refuses to ever apologize for anything non-PC he might do or say, because he knows what happens when you start giving in. But that's all.  

>On the content of his policies, he said that he wanted to stop illegal immigration, but that he would then welcome back those who want to respect the law, work and contribute to the country. He also proposed a temporary freeze on the immigration of Muslim foreigners, until Congress can set up an adequate vetting process to avoid islamists and jihadists from coming. First of all immigrating to the US is not a right that everyone in the world has; it is a privilege granted by the US Government. Secondly, it is just very natural prudence, not hatred. It's a fact that plenty of Muslims hate the US and would gladly commit atrocities there. A minority of them of course, but still more than among any other demographic, and for now you apparently can't tell well enough the radicals from the moderates. Also this policy of banning immigration from certain countries has a lot of precedent, including from Jimmy Carter. Finally even if he was prejudiced against Islam, it's an ideology to be judged and criticized, not a race that is in people's genes. And in my opinion, it does deserve a lot of criticism.  

>Trump is by the way officially endorsed by many minority figures, by the National Black Republican Association, and defended by the leader of Nation of Islam himself. And he polls quite well among minorities too.  

>>Trump is a far-right Republican  

>Absolutely not. Trump is a pragmatist about the economy and a moderate liberal on social issues. He's always been pro-LGBT, supporting civil unions giving the same rights as marriage, also in favour of anti-discrimination laws including in employment. He's criticized Kim Davis, wanted Caitlyn Jenner as a judge in his beauty pageant contests, and considers the question of gay marriage definitively settled by the Supreme Court. He had a change of mind on abortion and now considers himself pro-life, but he wouldn't try and ban it like most Republican candidates would (implying they could, which is false). Do you wonder why he hasn't yet revealed the details of his healthcare plans? It's pretty safe to guess he waits for the nomination first, because that plan is certainly much further to the left than what Republicans expect. In many ways Trump is to the left of Clinton. His economic and trade policy is entirely aimed at bringing back jobs for the blue-collar workers. He wants to lower taxes for the poor and end all loopholes for the rich and the multi-national corporations. He was the first to talk about corporate inversion, etc.  

>>Trump is an opportunist and a flip-flopper who says anything to get elected  

>There are a few things Trump says that are obvious pandering, and it's very easy to notice if you don't have brain damage. First of all when he talks about the Bible and Christianity. It's obvious he's not religious. And you'll notice he tries as hard as he can not to pretend and lie, which is why he prefers saying "Under President Trump people will say Merry Christmas again" rather than some of the actually threatening things other Republicans might say.
But then, what about the core of his message? Fixing the abysmal trade deficit, taking back jobs and defending the country's interests when negociating deals, instead of working as the world police for the benefit of multinational corporations. He has been saying the exact same things for 30 years. During this interview for instance.  

>I think I can believe him when he says that he's supported politicians for decades and decades but they always either lose or sell out to even bigger and richer special interests, so now he had enough and wants to fix the country by himself, without owing anything to donors.  

>Also, is Trump a flip-flopper on social issues? On abortion, as I said, he did have a change of mind. But otherwise, no. What changed is the majority opinion. When Trump campaigned for civil unions equal to marriage for gay people, he was more progressive than Hillary Clinton. Now when he says the exact same things he's considered a conservative or even a homophobe, simply because of the question of whether to call it "marriage" or not. Full disclosure, I'm gay, and yes this question is pretty irrelevant.  

>>Trump is stupid and speaks like a 4th grader  

>Trump is on the contrary very intelligent and extremely powerful at persuasion, as his poll numbers show. His speech is very much researched so as to be as persuasive as possible, by using very simple and strong words on purpose, repeating simple and short messages over and over, etc. We don't notice it because Trump has been practicing this for decades and decades and it has become natural for him. But if you watch old interviews when he's young he speaks like a well-educated person.  

>>Trump is a megalomaniac and a narcissist  

>First of all you might think of all the things he has put his name on, all the products he launched like Trump Ice or Trump Steak, or the Trump Game, or the Trump dolls, his own reality TV, etc. What do you think it means? Trump managed over the course of his life to change his name into an iconic brand, and most of those ridiculous projects were to put it simply, communication campaigns.  

>Also, a big part of Trump's philosophy of life (and one of the reasons for his business success) is his very optimistic thinking. It can be expressed by those rules: "Never apologize, never concede defeat or inferiority, never give up". Trump is the embodiment of all those "self-help" books that tell you how to become successful (or the ones he wrote): he convinces himself that he is absolutely great and special and can achieve anything he wants, and no obstacle or setback ever give him any doubt about his chances. That's why he brags so much.
But he is not a narcissist because he has absolutely no fear of being ridiculous. That's why he comes to TV to get roasted without a problem, or reads Jimmy Kimmel's mock-chidren book to his own grand-daughter. Other example, his hair: everyone makes fun of it, the Donald doesn't give a fuck, he keeps the same funny hairstyle and is proud of it.  

>>Trump would bring WWIII or something like that  

>Nonsense. Trump is for a big military and technological advantage over potential enemies, but he is absolutely not pro-war. He was opposed to the Irak War in 2003 for instance. And he is basically the only one advocating better relations with Russia, and cooperation with them in Syria to wipe out ISIS. Trump seems also like the best strategist to solve the ISIS situation quickly and restaure some stability. He was the first to propose to destroy their oil fields and so ruin them financially. He is now the only one advocating the creation of a safe protected zone in Syria to host the refugees, solving the crisis that has spread in Europe while protecting civilians. Others are also obsessed with the idea of a regime change in Syria, and give weapons to insurgents who will only make the war longer and bloodier, leading to chaos, anarchy and more refugees. I think that Trump, with his good strategy skills, his impressive capacity to persuade people, and his possible good relations with Putin, would easily negociate a deal for Syria, so that the State can rebuild in peace and stability and Bashar al-Assad can leave power to his right-hand without violence or trouble.  

##/u/Essen_scheisse  

>I am also a college student, Computer Science major. I'll try to just explain what I support, rather than why, because thats another discussion. Overall, I support the Don because he has realistic ideas for this country that are achievable. Anyways, here are some things off the top of my head:

>Some general policies from Trump that I support

>- I support putting an end to illegal immigration on the southern border.  
>- I support his lower overall income taxes to allow people to keep more of their own money. I also really like how he realizes people with less money need help and has a 0% tax bracket for that income level. I also support lower business taxes, again, to let businesses keep more of their own money.  
>- I support the 2nd amendment, something the Don defends vehemently.  
>- I support Donald's undying support of veterans. Several family members of mine are Iraq and Afghanistan veterans.  
>- I support his ideas on reforming trade involving: China, Mexico, Japan, etc. These countries are taking advantage of the United States economy and its time to change that.  

>In general  

>- Political correctness needs to die a painful death. The world is not a safe space and people need to realize that. The Don is the antithesis of PC.  
>- Trump is an extremely successful businessman. You don't get your name on buildings all over the world by being unsuccessful.  
>- Trump is an excellent deal maker. We don't need a president who will piss and moan like Obama every time he doesn't get exactly what he wants. The Don will be able to make deals to please both parties while also maintaining his goals.  

>Trump's entire campaign is a virtue for what he will do for this country. He has absolutely dominated these other politicians and has never done this before. I am just shocked at how much he has played the media for who they are and exposed the realm of politics for its bullshit. Just like Newt Gingrich said, "Trump knows what he doesn't know" in the sense that he can take something he has never seen before, and turn it into gold.
Trump is already getting along with foreign leaders. People can say what they want, but what Putin said about Trump is very good for relations with Russia.  

>Anyways, I may edit this post later to add more but I have to leave at the moment. Let me know what you think.  

##/u/OnlyFactsMatter  

>**Reason 1**: The Wollman Rink was a heavily used public skating rink which had fallen into disrepair in 1980. New York City tried for six years to fix it, spent $13 million, and the rink still was not ready to open.
In June of 1986 Trump, who could see the rink from his apartment, finally got tired of the embarrassment and offered to fix the rink at his own expense.  
  
>At first the city turned him down because its bureaucracy did not want to be embarrassed by someone fixing something they couldn’t fix. Trump kept pushing and finally out of embarrassment the city gave in.
The key part of the story is Trump’s reaction to being put in charge. He promptly recognized that he didn’t know anything about fixing a skating rink. He asked himself who built a lot of skating rinks. “Canadians!” he concluded. He found the best Canadian ice skating rink construction company.  

>When the Canadians flew in to assess the situation, they were amazed at how bad the city had been at solving the problem. They assured Trump that this was an easy job.  

>Trump fixed the six year embarrassment two months ahead of schedule and nearly $800,000 under-budget. (The city did end up paying for the work, and Trump donated the profits to charity.)
After reading this chapter you begin to think that maybe Donald Trump really could build a wall along our southern border for a lot less than our current government estimates.  

>NAFTA is an executive agreement. Congressional action on NAFTA has permitted the President to enter into these agreements. This means that the President has the authority to renegotiate these agreements under the trade acts passed by Congress in the 70's and 80's.  

>NAFTA has no expiration date but it must be periodically renegotiated. NAFTA benefits Mexico and big business in the USA and Donald Trump would be in a huge position to put Mexico in a vice over it. Mexico would gladly build a wall with their NAFTA trade surplus.  

>And bear in mind that he would have the backing of the AFL-CIO on this issue which may effect a massive Republican landslide in the general election. Trump is on the verge of becoming an unstoppable juggernaut.
When he says there will be a big, beautiful wall you'd better believe him.  

>**Reason 2** China is forcing Boeing to open a manufacturing plant in China. But not only is China demanding Boeing open a plant in China, but China is refusing to trade with Boeing unless the company moves.  

>You think China cares about putting people to work? LMAO!!! It's about China importing their research and development, Boeing's production secrets, into their country so they can learn, steal, and begin to manufacturer their own airliners.  

>In time, Comac, a state owned, Shanghai-based aerospace company will then use the production secrets they have stolen, produce their own airliners, kick out Boeing, undercut the market, and sell cheaper manufacturer airplanes to the global economy.   

>Boeing is now just another notch on the ASian market belt. All of those Boeing workers, those high-wage industrial skill jobs that support the American middle class..... gone. Just like that. And then Wall Street will be invested in the cheaper Chinese aerospace manufacturing company Comac, as it emerges as a manufacturing power.
Trump called them out on this, and Bush's response was perfect: "C'mon man..." It shows the disconnect these politicians have with people. Unless these politicians get their talking poins from their donors, they are useless.  

>You can help Make America Great Again by voting....  

>TRUMP 2016!!!!!!!!  

##/u/giulez845  

>What draws me to Trump is that in my opinion he's the closest thing to an independent nationalist. Our country is trapped in the age old "republican vs democrat"/ "conservative vs liberal" narrative for quite some time which in my opinion impedes meaningful solutions to our problems and keeps people divided among themselves. I've summarized 5 reasons to vote for the Don below.  

>- Trump will, unlike Bernie, encourage free trade, adjust taxes, and encourage american corporations to bring jobs back to our country. This is a huge step at allowing American jobs to flourish. Bernie seems to point a majority of our problems at Wall Street, which is not wrong, however not the root, nor the solution to the problem.
Trump is the ONLY candidate who is (for the most part) completely self funding his entire campaign. This means that there are no millionaire/billionaire CEO's, politicians, or other people who pay off and fund campaigns in order to get politicians into office for a deal. (Some claim that Bill Gates/Microsoft are backing Rubio to push for more H-1B visas)  

>- Trump is incredibly in touch with reality unlike political bureaucrats and some other candidates election-wide. I say this because he's been constantly talking about not only our own problems, but problems across the globe, including the European migrant crisis. He's been saying for months he wants to pressure the gulf states AND European nations to build safe zones for refugees of the Syrian civil war and waves of millions of migrants entering Europe, causing an international crisis and outcry from European citizens.  

>- Not sure what your experience with religion is like, but here goes. Trump isn't waving the bible around and citing it as the basis of his campaign (Cruz), yet isn't afraid to say that Christianity is under attack from ISIS in Iraq and Syria. He's not afraid to explicitly say that while no ground troops are needed or desired by the people, ISIS needs to be eradicated and wiped off the face of the earth, for obvious reasons, but also so that the migrants can return from Europe and help rebuild their own country.  

>- Lastly, Trump is not politically correct. He has a tremendous ego which is required in many leadership roles, like it or not. The man has transformed his father's business into a billion dollar company. He knows how to make smart deals, he know's what's in his interest AND America's interest. Ultimately, he's a leader, and a no holds barred, rugged and cunning one at that. Half these candidates (including Bernie) haven't ever successfully run a lemonade stand on the street they grew up on. I believe he genuinely loves this country and it's citizens, and has it's best interests at heart.  