 


### **Introduction**

Welcome to the /r/The_Donald Wiki, where we try to outline why one would want to vote for Trump and his direct positions laid out in his book *Time To Get Tough: Making America #1 Again* and on the internet. This wiki will also contain sources and data that supports Trump's positions.

___
___
___



### **Why Would We Support Trump?**

This is a broad question. There are many reasons one would want to vote for a candidate. Some people might be **single issue voters** which is defined as a vote being dependent on a certain position for a particular policy. Some might be single issue for Trump for his stance on illegal immigration. *Note: Being anti-illegal immigration is not equal to being xenophobic or anti immigration, it is simply a choice to enforce our own laws for citizenship, but we will dive more into illegal immigration under Trump's policies.*

Many people, those who ponder why someone would want to vote for Trump, consider him a racist, sexist, and a xenophobe who ran as a joke. If you happen to think this, first **compartmentalize that right now and proceed with an open mind**. It is very important for you to do so or else this won't be beneficial for either parties involved. 



Now that we have discussed a few reasons *why* someone would vote for Trump, let's go deeper and examine his policy positions. To begin, we'll be looking at his 2011 book, *Time To Get Tough: Making America #1 Again*; essential reading for any Trump supporter. This will not only show Trump's consistency on various positions over the past 4.5 years, but it will also provide even more evidence as to why Trump has the best platform out of all the candidates. The following sections will be organized by chapter with the exception of illegal immigration which will be first. *Sources used by Trump will be linked in each section.*

*Note: As this book is almost five years old, there is likely to be minor changes to specifics within each policy. For example, the tax brackets in his income tax plan are slightly different. This is likely to be the case for other policies in the book, but like his tax plan, it still gives an excellent idea of what one can expect in that particular policy in a Trump policy.*

Also, see our subsection on [Why Donald?](https://www.reddit.com/r/The_Donald/wiki/whydonald) for some specific examples and reasons for voting Trump.

____
____
____

### **Frequently Asked Questions**

#### **What do all of those terms / memes mean?**

* *Low Energy* - A "kill shot" aimed at Jeb Bush (a.k.a "[guac bowl merchant](http://archive.vn/0n3Qt)"). Jeb was simply low energy and the nickname Trump made up stuck. High energy is the opposite of low energy. You want to be high energy.

* *Coats* - A Bernie supporter crashed a Trump rally in the winter and Trump made a joke, ["take his coat and we'll mail it back,"](https://www.youtube.com/watch?v=Bb2GzMstrMk) like he was going to throw the protester out in the cold. If you're new to the subreddit and become a Trump supporter, you're welcome to have a coat. If you're a Bernie Cuck or Hillshill however, we will be confiscating your coat.

* *Cuck* - Shorthand for "cuckold". A cuck gets off on his wife getting fucked by another man. A cuck in the political sense which we use here is someone who sits idly or encourages policies which fuck their country, national identity, and cultural heritage. A cuckservative gets off on watching liberals fuck America.

* *Schlonged* - A term that means "beat badly". Trump used that term to describe Hillary's defeat by Obama. Hillary tried to say it was sexist, of course she was wrong.

* *Yuge* - A play on how Trump says "huge".

* *Bigly* - A play on how Trump says "big league". 

* *Centipede* - Who we are. Refers to [Knife Party's Song - *Centipede*](https://www.youtube.com/watch?v=vMmtfmYqk34) and it's used in the [Can't Stump The Trump](https://www.youtube.com/channel/UCoWIVzxp6oLjsGMGuzzmxig) video series.

* *'pede* - See "Centipede" above.

* *Nimble Navigator* - Same as centipede from [Can't Stump The Trump Guy.](https://www.youtube.com/channel/UCoWIVzxp6oLjsGMGuzzmxig) Watch the beginning of any of the later Can't Stump The Trump videos and you'll see centipede and nimble navigator in the opening song.

* *CTR* - Correct The Record, Hillary Clinton's Stormtroopers on the social media front, created by Mrs. Doubtfire David Brock. We defeated them in the first great MEME war, but now they have been revived under a new name "ShareBlue", it serves only one purpose is to keep other people from being redpilled by r/The_Donald.

* *ShareBlue* or *ShariaBlue* - David Brock (same guy who runs *Correct the Record*) developed a new organization to carry on CTR's work in a broader variety of forums with new tactics.  They, along with CTR, still stalk The_Donald to this day, albeit with lower activity than they had during and just after the election.  Third-party evidence points to up to 30% of all reddit's traffic potentially being from astroturfed sources.

* *Bill Clinton is a rapist - infowars.com* - During the campaign, Alex Jones of [infowars.com](http://infowars.com/) offered a cash reward to anyone who could would hold a sign calling attention to the fact that Bill Clinton has been involved in numerous sexual misconduct allegations over a period of many decades.  This lead to several people wearing shirts that read "BILL CLINTON IS A RAPIST" -- often with the infowars logo -- being filmed at various events that left-wing politicians were patronizing.

* *Autist* - a skilled information gather/compiler who is able to sort through massive amounts of data to get to the truth.

* *Brigading* - when a rash of shills invade this DOMreddit.

* *Bye Felicia* - From the Comedy Film "Friday", meaning a dismissive farewell to someone who had unsuccessfully tried to pull something over on you.

* *GEOTUS* - God Emperor of The United States - Used to describe only President Trump.

* *Kek* - from 4chan /pol/, the God of shitposters, the supreme Diety (origin- ancient Egypt) - also used as a synonym for "lol".

* *Kekistan* - the fictional country of shitposters. (See also: Kek) Flag of Green with black and white lines and circles.

* *Meme Magic* - when a meme "transcends the realm of cyberspace and results in real life consequences".

* *Pearl Clutching* - a reference to 1960's movie theme of women clutching their pearl necklaces when fearful or agitated. An "Oh dear, oh dear, The Sky is Falling" attitude or comment.

* *Pepe* - the Green Frog who is the prophet of Kek.

* *Shill* - any number of paid and/or unpaid individuals who infiltrate The_Donald DOMreddit in order to undermine our support of President Trump.

* *The Trump Curse* - When an individual disparages President Trump his family or his policies and subsequently has it blow up in their face or some other bad luck befalls them.



#### **Can you explain your rules?** 

1. No Spamming - See [Reddit's Content Policy](https://reddit.zendesk.com/hc/en-us/articles/204536499) for an explanation of what is considered spam.

2. No Trolling / Concern Trolling - Trolling is defined as, "sowing discord on the Internet by starting arguments or upsetting people, by posting inflammatory, extraneous, or off-topic messages in an online community." Concern trolling is a variation of trolling wherein the troll attempts to present themselves as though they are a part of the community who is simply concerned.
 
3. No Racism or Anti-Semitism - Posts containing attacks based on race or [anti-semitism](https://en.wikipedia.org/wiki/Antisemitism) are not allow. Bear in mind that Muslim and illegal immigrant are not races.

4. No Releasing of Personal Information - Per the [Reddiquette guidelines](https://www.reddit.com/wiki/reddiquette), do not post someone's personal information, or post links to personal information. This includes links to public Facebook pages and screenshots of Facebook pages with the names still legible. Additionally, on pages such as Facebook, where personal information is often displayed, please mask the personal information and personal photographs using a blur function, erase function, or simply block it out with color.

5. No Vote Manipulation/Brigading - Brigading is defined as, "a deliberate attempt by one party to collectively manipulate the content of another." This is not limited to Reddit. Reddit considers this behavior harassment, and have removed many subs in the past for the doing so. Admins are very strict with Vote Manipulation eg. simply upvoting or downvoting a single post on two or more accounts. Typically you get permanently banned right away - so don't do it.

6. Trump Supporters ONLY - This sub is for supporters of Donald J. Trump ONLY. This is not a place for you to debate with us about Donald Trump, or to ask us to convince you to like Donald Trump. This is not a neutral place - we are 100% in support of Donald J. Trump. Moderators reserve the right to ban non-supporters as we see fit.

7. No Posts Related to Being Banned From Other Subreddits - Self explanatory.

8. Ban appeals, suggestions, concerns (including sticky choices) go to modmail - ANY Meta-content must go to modmail. This includes "mods please sticky," or "we should do X," or anything of the sort. Ban appeals **must** go to modmail, or they will be ignored. **Do not** send ban appeals via PM to any mod.

9. Don't Post People Planning to Assassinate Trump - If you witness this behavior online, [feel free to report it to the FBI](https://www.fbi.gov/report-threats-and-crime).

10. Please do not behave in a way outside of the subreddit that would reflect poorly on it. - This includes private messages or posts on Reddit, or any other website/location. The moderators of the_donald reserve the right to ban anyone we feel is engaging in harassing behavior either on the_donald or elsewhere. This rule also includes "brigading," or "manipulating," the votes on any other subreddit.

#### **"Can you tell me more about Trump and  ______?"**

* ##### [The Wall](http://archive.vn/H47IT)

* ##### [Racism/Misogynism/Anti-LGBT?](http://archive.vn/8K68W)

* ##### [Muslims](http://archive.vn/uFgUg)

* ##### [Healthcare Reform](http://web.archive.org/web/20170104151119/https://www.donaldjtrump.com/positions/healthcare-reform)

* ##### [US-China Trade Relations](http://web.archive.org/web/20161202034538/https://www.donaldjtrump.com/policies/trade/?/positions/us-china-trade-reform)

* ##### [Veterans](http://web.archive.org/web/20161227164548/https://www.donaldjtrump.com/policies/veterans-affairs-reform/?/positions/veterans-administration-reforms)

* ##### [Tax Reforms](http://web.archive.org/web/20161124152702/https://www.donaldjtrump.com/policies/tax-plan/?/positions/tax-reform)

* ##### [2nd Amendment Rights](http://web.archive.org/web/20161130190255/https://www.donaldjtrump.com/policies/constitution-and-second-amendment/?/positions/second-amendment-rights)

* ##### [Immigration](http://web.archive.org/web/20161130190243/https://www.donaldjtrump.com/policies/immigration/?/positions/immigration-reform)

* ##### [Pepe the Frog](/comments.php?id=53f0cl)

* If you can't find the answer to your question here, check out /r/AskThe_Donald!

* All official positions listed on [Trump's website](http://web.archive.org/web/20161108053904/https://www.donaldjtrump.com/policies/)

___
___
___

### **[List of AMAs](https://www.reddit.com/r/The_Donald/wiki/amas)**

___
___
___

### Noteworthy Articles 

[Would Trump's Trade Policy Really Cause a Recession?](/comments.php?id=49qhmg)

[The Reformed BernieBot's Guide on Assimilation!](/comments.php?id=4apox3)

[How to Convince #NeverTrump People to Change Their Minds.](http://archive.vn/sQ5rC)

[Detailed analysis and evidence showing Guccifer 2.0 was a misdirection effort to falsely blame Russian hackers for the DNC leaks.](http://archive.vn/YNhrF)

[Trump, Russia and the Media - a very thorough analysis of recent allegations regarding Trump and Russia](http://archive.vn/ZBOWM)

___
___
___

### The Clinton Crime Family:


#####Benghazi

- [Video Montage Shows Obama, Hillary, Susan Rice and Jay Carney All Blaming Benghazi on the Video](http://web.archive.org/web/20170428195517/https://www.theblaze.com/stories/2014/05/02/video-montage-shows-obama-hillary-susan-rice-and-jay-carney-all-blaming-benghazi-on-the-video/)

- [Hillary Clinton and Obama’s Lies on Benghazi — Too Many to Count, but Let’s Try](http://archive.vn/hXuZB)

- [Benghazi Heroes Endorse Donald Trump](http://archive.vn/ET3eR)

- [Benghazi Victim’s Mother: ‘Special Place In Hell’ For People Like Hillary, “I Hope She Enjoys It There’](http://archive.vn/UQHtC)

##### Emails

- [Top 100 Wikileaks Emails](http://archive.vn/h51v4)

- [Hillary Clinton Chastises the Bush Administration for Using Secret Email Accounts](https://www.youtube.com/watch?v=-En_DyJWsso&feature=youtu.be)

- [Wikileaks database of Hillary Clinton's emails](https://wikileaks.org/clinton-emails/)

- [FORMER 12-YEAR-OLD RAPE VICTIM: "HILLARY CLINTON TOOK ME THROUGH HELL"](http://archive.vn/0DHcH)

##### Ties To Racists

- [Hillary Clinton Praises Member Of The Ku Klux Klan](http://web.archive.org/web/20160321202455/http://www.hannity.com/articles/election-493995/watch-hillary-clinton-praises-member-of-14435828/)

- [Bill Clinton endorsing former KKK leader Robert Byrd](bill_byrd.jpg)

- [Ku Klux Klan Grand Dragon Will Quigg Endorses Hillary Clinton for President](http://archive.vn/fWqqK)

##### Misc

- [Politifact: Clinton said "great things about NAFTA until she started running for president." = TRUE](http://archive.vn/JVlyv)

- [The Hole in Hillary’s Flip-Flop Excuse: She keeps saying new information makes her change her mind on policy. But what new information?](http://archive.vn/fOD11)

- [Hillary Clinton's fifteen biggest scandals](http://archive.vn/TjiEK)

###Crooked Hillary's 10 Legendary Lies

#### 1. Hillary Lies to Benghazi Families

#### 2. Hillary Lies About Classified Material on her Secret Server

#### 3. Secret Server-Hillary Broke the Rules & Lied About It

All information on Hillary's Legendary Lies can be found at [lyingcrookedhillary.com](http://web.archive.org/web/20170218014739/https://www.lyingcrookedhillary.com/)

___
___
___

### **FAQs for dealing with Sanders Supporters and Other Trolls**

* [MUST READ post for Sanders Supporters Lurking](/comments.php?id=4allva)

* [History of Sanders's Involvement in Stalinist/Leninist Organizations](http://web.archive.org/web/20170310125914/http://www.discoverthenetworks.org/individualProfile.asp?indid=2266)

* [Debunking Anti-Trump Post ("Donald Trump is not the alternative to Senator Sanders, and you need to know why")](/comments.php?id=4atcqz)

* [Detailed Analysis of Sanders' "bat shit crazy" tax plan](http://archive.vn/ysmt4)

* [**Rebuttal Cheat Sheet**](/comments.php?id=49qtyl)

* [**Scott Adam's  Rebuttal  Cheat Sheet**](http://archive.vn/sQ5rC)

* [Common Trump Myths and Facts Debunked](http://web.archive.org/web/20170202044050/http://trumpgeneral.com/myths)
 
**Informative Video Sources**

* [Informative Video For Freedom of Speech In America](https://youtu.be/9vVohGWhMWs)

* [Video Dispelling SJW Rape Culture Argument](https://youtu.be/K0mzqL50I-w)

___
___
___

### **Magathread of Compiled Pictures/Videos of Violent Protesters**


* [Thread](/comments.php?id=4mcj61)

Please note that this is a work in progress. There is still much to add here and the mod team is working on it. If you have any suggestions, don't hesitate to message us.


___
___
___


#President Trump's Cabinet

**Vice President of the United States**

[Michael Richard "Mike" Pence](https://en.wikipedia.org/wiki/Mike_Pence)

**Department of State**

[Secretary Michael Richard Pompeo](https://en.wikipedia.org/wiki/Mike_Pompeo)

state.gov

**Department of the Treasury**

[Secretary Steven Terner Mnuchin](https://en.wikipedia.org/wiki/Steven_Mnuchin)

treasury.gov

**Department of Defense**

[Acting Secretary of Defense Patrick M. Shanahan](https://en.wikipedia.org/wiki/Patrick_M._Shanahan)

defense.gov

**Department of Justice**

[Attorney General William P. Barr](https://en.wikipedia.org/wiki/William_Barr)

usdoj.gov

**Department of the Interior**

[Acting United States Secretary of the Interior David Bernhardt](https://en.wikipedia.org/wiki/David_Bernhardt)

doi.gov
 

 
**Department of Commerce**

[Secretary Wilbur Louis Ross, Jr.](https://en.wikipedia.org/wiki/Wilbur_Ross)

commerce.gov

**Department of Labor**

[Secretary Rene Alexander "Alex" Acosta ](https://en.wikipedia.org/wiki/Alexander_Acosta)

dol.gov

**Department of Health and Human Services**

[Secretary Alex Michael Azar II](https://en.wikipedia.org/wiki/Alex_Azar)

hhs.gov
 
**Department of Housing and Urban Development**

[Secretary Benjamin Solomon "Ben" Carson, Sr.](https://en.wikipedia.org/wiki/Ben_Carson)

hud.gov
 
**Department of Transportation**

[Secretary Elaine Lan Chao](https://en.wikipedia.org/wiki/Elaine_Chao)

dot.gov
 
**Department of Energy**

[Secretary James Richard "Rick" Perry](https://en.wikipedia.org/wiki/Rick_Perry)

energy.gov
 
**Department of Education**

[Secretary Elisabeth "Betsy" DeVos](https://en.wikipedia.org/wiki/Betsy_DeVos)

ed.gov
 

 
**Department of Homeland Security**

[Acting Secretary Kevin K. McAleenan](https://en.wikipedia.org/wiki/Kevin_McAleenan)

dhs.gov

 
**White House Chief of Staff**

[Acting White House Chief of Staff Mick Mulvaney](https://en.wikipedia.org/wiki/Mick_Mulvaney)

**Environmental Protection Agency**

[Acting Administrator Andrew R. Wheeler](https://en.wikipedia.org/wiki/Andrew_R._Wheeler)

epa.gov

**United States Mission to the United Nations**

[Acting U.S. Ambassador Jonathan Cohen](https://en.wikipedia.org/wiki/Jonathan_Cohen_(diplomat))

usun.state.gov


**Office of Management & Budget**

[Director John Michael "Mick" Mulvaney](https://en.wikipedia.org/wiki/Mick_Mulvaney)

whitehouse.gov/omb

**United States Trade Representative**

[Robert Emmet Lighthizer](https://en.wikipedia.org/wiki/Robert_Lighthizer)

ustr.gov


**Council of Economic Advisers**

Chairman: [Kevin Hassett](https://en.wikipedia.org/wiki/Kevin_Hassett)

https://www.whitehouse.gov/cea/

**Department of Agriculture**

[Secretary Sonny Perdue](https://en.wikipedia.org/wiki/Sonny_Perdue)


usda.gov

**Department of Veterans Affairs**

[Secretary David Shulkin](https://en.wikipedia.org/wiki/David_Shulkin)

va.gov
 
**Small Business Administration**

[Administrator Linda Marie McMahon](https://en.wikipedia.org/wiki/Linda_McMahon)

sba.gov


**Chief Strategist** 

Vacant


**CIA Director**

[Director Gina Cheri Haspel ](https://en.wikipedia.org/wiki/Gina_Haspel)


**National Security Advisor**



[John Robert Bolton](https://en.wikipedia.org/wiki/John_R._Bolton)



**White House Counsel**

[Pat A. Cipollone](https://en.wikipedia.org/wiki/Pat_Cipollone)