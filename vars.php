<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

$tdaVersion = "1.0.0";

$dataRoot = "/m/Projects/reddit-archive/";
$htmlRoot = "/var/www/html/";

$useZip = true;
$useZipThumbs = false;

//parse ini file
$f = $_SERVER['DOCUMENT_ROOT'] . '/tda.ini';
if(file_exists($f)) {
	//$ini = parse_ini_file($f, false, INI_SCANNER_TYPED);
	$ini = parse_ini_file($f, false, INI_SCANNER_RAW);
	
	#process vars
	if(isset($ini['dataroot'])) {
		$dataRoot = $ini['dataroot'];
	}
	if(isset($ini['htmlroot'])) {
		$htmlRoot = $ini['htmlroot'];
	}
	if(isset($ini['usezip'])) {
		$useZip = (strcasecmp($ini['usezip'], 'true') == 0);
	}
	if(isset($ini['usezip-thumbs'])) {
		$useZipThumbs = (strcasecmp($ini['usezip-thumbs'], 'true') == 0);
	}
	if(isset($ini['usezip-imgur'])) {
		$useZipImgur = (strcasecmp($ini['usezip-imgur'], 'true') == 0);
	}
	if(isset($ini['usezip-reddit'])) {
		$useZipReddit = (strcasecmp($ini['usezip-reddit'], 'true') == 0);
	}
	if(isset($ini['usezip-reddituploads'])) {
		$useZipRedditUploads = (strcasecmp($ini['usezip-reddituploads'], 'true') == 0);
	}
	if(isset($ini['usezip-slimg'])) {
		$useZipSlimg = (strcasecmp($ini['usezip-slimg'], 'true') == 0);
	}
	if(isset($ini['usezip-magaimg'])) {
		$useZipMagaimg = (strcasecmp($ini['usezip-magaimg'], 'true') == 0);
	}
	if(isset($ini['usezip-misc'])) {
		$useZipMisc = (strcasecmp($ini['usezip-misc'], 'true') == 0);
	}
	if(isset($ini['usezip-giphy'])) {
		$useZipGiphy = (strcasecmp($ini['usezip-giphy'], 'true') == 0);
	}
	if(isset($ini['usezip-twimg'])) {
		$useZipTwimg = (strcasecmp($ini['usezip-twimg'], 'true') == 0);
	}
}

/*
print_r($ini);

if($useZipThumbs == true) {
	echo '<!--===== ' . $useZipThumbs . ' ====-->';
} else {
	echo '<!--===== NO MATCH ====-->';
}

echo '<!--===== ' . $useZipThumbs . ' ====-->';

*/


$reddDatabase = $dataRoot . "reddit-archive.db";



?>