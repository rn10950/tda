function updateCaption() {
	// define captions
	var captions = [
		"Default Theme",
		"Theme 1",
		"Theme 2",
		"Theme 3",
		"Theme 4",
		"Theme 5",
		"Theme 6",
		"Theme 7",
		"Theme 8",
	];
	$( "#theme-slide-caption" ).text(captions[this.currentSlide]);
}

$( document ).ready(function() {
	const mySiema = new Siema({
	  selector: '.siema',
	  perPage: 1,
	  startIndex: 0,
	  draggable: false,
	  multipleDrag: false,
	  threshold: 20,
	  loop: false,
	  rtl: false,
	  onInit: updateCaption,
	  onChange: updateCaption,
	});
	
	const prev = document.querySelector('.prev');
	const next = document.querySelector('.next');

	prev.addEventListener('click', () => mySiema.prev());
	next.addEventListener('click', () => mySiema.next());

});
