$( document ).ready(function(){
	// collapsible comments
	$( '.expand' ).click(function(){
		if($( this ).text() == "[–]"){
			$( this ).text("[+]"); 
		} else if($( this ).text() == "[+]"){
			$( this ).text("[–]"); 
		}
		$( this ).closest( ".comment" ).toggleClass( "noncollapsed collapsed" );
		$( this ).closest( ".comment" ).find( ".usertext-body" ).toggleClass( "TDAhidden" );
	});
});