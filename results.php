<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once("functions.php");

$database = $reddDatabase;


$debug = false;
$listPage = true;

if($debug == true) {
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	echo "<pre>";
	print_r($_GET);
	echo "</pre>";
} else {
	echo "<!--";
}


// determine display mode
if(isset($_GET['dateTime']) || isset($_GET['tm'])){
	// date/time mode
	$hTime = true;
	$tm = true;
	if(isset($_GET['tm']) == false || $_GET['tm'] == 1) {
		// mm/dd/yyyy format
		$dt = strtotime($_GET['dateTime']);
	}else if($_GET['tm'] == 0) {
		// calendar mode
		// Y/m/d H:i
		$dt1 = DateTime::createFromFormat('Y/m/d H:i', $_GET['dateTimeCal'], new DateTimeZone('America/New_York'))->getTimestamp();
		$dt = $dt1;
	} else if($_GET['tm'] == 2) {
		// epoch mode
		$dt = $_GET['dateTimeEpoch'];
	} else {
		$dt = 1478656800;
	}
	$thDate = $dt;
	$minDt = $dt - 86400; // start time 1 day before
	$searchQuery = "SELECT * FROM submissions WHERE created_utc < " . SQLite3::escapeString($dt) . " AND created_utc > " . SQLite3::escapeString($minDt) . " ORDER BY score DESC LIMIT 25 ";
	if(isset($_GET['offset'])){
		$pp = 25;
		$offset = (int)$_GET['offset'];
		$searchQuery = $searchQuery . "OFFSET " . SQLite3::escapeString($offset);
	} else {
		$pp = 25;
		$offset = 0;
	}
	if($debug) {
		echo '<h1>' . $dt . '</h1>';
	}
} else if (isset($_GET['top'])){
	// top mode
	$hTop = true;	
	$searchQuery = "SELECT * FROM submissions ORDER BY score DESC LIMIT 25 ";
	if(isset($_GET['offset'])){
		$pp = 25;
		$offset = (int)$_GET['offset'];
		$searchQuery = $searchQuery . "OFFSET " . SQLite3::escapeString($offset);
	} else {
		$pp = 25;
		$offset = 0;
	}
} else {
	//search
	$hSearch = true;
	$q = [];

	array_push($q, "SELECT * FROM submissions WHERE ");

	$qarr = explode(" ", $_GET['q']);
	foreach($qarr as $a) {
		array_push($q, '"title" LIKE "%' . SQLite3::escapeString($a) . '%" AND ');
	}

	if($_GET['chkTextPost'] == true) {
		$l = key(array_slice($q, -1, 1, true));
		$q[$l] = str_replace("AND ", "OR ", $q[$l]);
		foreach($qarr as $a) {
			array_push($q, '"selftext" LIKE "%' . SQLite3::escapeString($a) . '%" AND ');
		}
	}

	if($_GET['chkStartDate'] == true) {
		array_push($q, "created_utc > " . SQLite3::escapeString(strtotime($_GET['startDate'])) . " AND ");
	}

	if($_GET['chkEndDate'] == true) {
		array_push($q, "created_utc < " . SQLite3::escapeString(strtotime($_GET['endDate'])) . " AND ");
	}


	if($_GET['chkMinScore'] == true) {
		array_push($q, "score > " . SQLite3::escapeString($_GET['minScore']) . " AND ");
	}

	if($_GET['chkMaxScore'] == true) {
		array_push($q, "score < " . SQLite3::escapeString($_GET['maxScore']) . " AND ");
	}


	if($_GET['chkMinCC'] == true) {
		array_push($q, "num_comments > " . SQLite3::escapeString($_GET['minCC']) . " AND ");
	}

	if($_GET['chkMaxCC'] == true) {
		array_push($q, "num_comments < " . SQLite3::escapeString($_GET['maxCC']) . " AND ");
	}


	if($_GET['chkDomain'] == true) {
		array_push($q, 'domain LIKE "' . SQLite3::escapeString($_GET['domain']) . '" AND ');
	}


	if($_GET['type'] == "self") {
		array_push($q, "is_self = 1 AND ");
	} else if($_GET['type'] == "link") {
		array_push($q, "is_self = 0 AND ");
	}

	// remove last 'AND'
	$l = key(array_slice($q, -1, 1, true));
	$q[$l] = str_replace("AND ", "", $q[$l]);

	/// SORT ///
	array_push($q, "ORDER BY " . SQLite3::escapeString($_GET['sort']) . " " . SQLite3::escapeString($_GET['sortDir']) . ' ');
	
	
	/// case sensitivity ///
	//array_push($q, "COLLATE NOCASE");

	/// PAGENATION ///
	if(isset($_GET['perpage'])) {
		array_push($q, "LIMIT " . SQLite3::escapeString($_GET['perpage']) . " ");
		$pp = $_GET['perpage'];
	} else {
		array_push($q, "LIMIT 25 ");
		$pp = 25;
	}
	if(isset($_GET['offset'])) {
		array_push($q, "OFFSET " . SQLite3::escapeString($_GET['offset']) . " ");
		$offset = $_GET['offset'];
	} else {
		$offset = 0;
	}
	
	// raw query mode
	if($_GET['chkQuery'] == true) {
		$searchQuery = $_GET['query'];
	} else {
		$searchQuery = implode("", $q);
	}
}




//if($debug == true) { echo "-->"; }

echo "<pre>$searchQuery</pre>";

/// establish database connection
$db = new SQLite3($database);
$results = $db->query($searchQuery);
$i = 0;
$posts = [];
while($r = $results->fetchArray()) {
	array_push($posts, $r);
}

if($debug == false) {
	echo "-->";
}

require "settingsLoader.php";

/////////////// ============[ START GENERATING PAGE ]============ ///////////////
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<?php
	if($tm) {
		if(date('H:i', $dt) == "00:00"){
			echo '<title>The_Donald Archive - ' . date('M jS, Y', $dt) . '</title>';
		} else {
			echo '<title>The_Donald Archive - ' . date('M jS, Y @ g:i A', $dt) . '</title>';		
		}
	} else if(isset($_GET['top'])) {
		echo '<title>Top - The_Donald Archive</title>';
	} else {
		echo '<title>' . $_GET['q'] . ' - Search Results - The_Donald Archive</title>';
	}
	?>
	<link rel="stylesheet" type="text/css" href="/reddit-posts.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
	<link rel="stylesheet" type="text/css" href="/tda.css">
	<style>
		.link {
			display: table-row !important;
		}
		
		.TDAhidden {
			display: none !important;
		}
	</style>
	<script src="/jquery.min.js"></script>
	<script src="/results.js"></script>
</head>
<body>
<?php
require("header.php");
// generate html for links
foreach($posts as $r) {
	$url = urlProc($r['url'], $r['domain']);
	
	//$thumb = "/pepe.jpg";
	
	
	
	// youtube
	if($r['domain'] == 'youtube.com' || $r['domain'] == 'youtu.be') {
		preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
		$youtube_id = $match[1];
		$isYT = true;
		//$thumb = "/youtube.jpg";
	} else { $isYT = false; }
	
	// twitter
	if($r['domain'] == 'twitter.com') {
		//$thumb = "/twitter.png";
	}
	
	// images and thumbnails
	if(endsWith($url, ".jpg") || endsWith($url, ".jpeg") || endsWith($url, ".gif") || endsWith($url, ".gifv") || endsWith($url, ".png") || endsWith($url, ".bmp") || strpos($url, "/imageproc/") !== false ) {
		$isImg = true;
		//$thumb = $url;
	} else {
		$isImg = false;
	}
	
	//thumbnail
	$thumbU = $r['thumbnail'];
	if($thumbU == "default") {
		$thumb = "/pepe.jpg";
		if($isYT){
			$thumb = "http://img.youtube.com/vi/" . $youtube_id . "/sddefault.jpg";
		} else if ($r['domain'] == "i.imgur.com" 
			|| $r['domain'] == "imgur.com"
			|| $r['domain'] == "i.magaimg.net"
			|| $r['domain'] == "magaimg.net"
			|| $r['domain'] == "i.sli.mg"
			|| $r['domain'] == "i.redd.it"
			|| $r['domain'] == "i.reddituploads.com"
			|| $isImg) {
				$thumb = $url;
			}
	} elseif ($thumbU == "self") {
		$thumb = "/pepe.jpg";
	} else {
		$thumbId = get_string_between($thumbU, ".com/", ".jp");
		$thumb = '/imageproc/thumb.php?id=' . $thumbId;
	}
	
	?>	
		<div class="thing link">
			<!--<p class="parent"></p>
			<span class="rank">2</span>-->
			
			<div class="midcol unvoted">
				<div class="arrow up" tabindex="0"></div>
				<div class="score unvoted" title="<?php echo $r['score']; ?>"><?php echo $r['score']; ?></div>
				<div class="arrow down" tabindex="0"></div>
			</div>
			<?php
			if($r['is_self'] == 1) {
				echo '<a class="thumbnail invisible-when-pinned self may-blank "></a>';
			} else {
				echo '<a class="thumbnail invisible-when-pinned may-blank "><img src="' . $thumb . '" alt="" width="70" height="70"></a>';
			}
			
			?>
			<div class="entry unvoted">
				<div class="top-matter">
					<p class="title"><a class="title may-blank " href="<?php echo $url; ?>" tabindex="1"><?php echo $r['title']; ?></a>
					<?php
						// show link flair if present
						if(isset($r['link_flair_css_text']) && strlen($r['link_flair_css_text']) > 0) {
							echo '<span class="linkflairlabel ' . $r['link_flair_css_class'] . '" title="' . $r['link_flair_css_text'] . '">' . $r['link_flair_css_text'] . '</span>';
						}
					?>					
					<span class="domain">(<a href="#SEARCH BY DOMAIN"><?php echo $r['domain']; ?></a>)</span></p>
					<?php
						// add expando if needed
						if($r['is_self'] == 1 && strlen($r['selftext']) > 0){
							echo '<div class="expando-button collapsed hide-when-pinned selftext"></div>';
						} else if ($isImg == true || $isYT == true) {
							echo '<div class="expando-button collapsed hide-when-pinned video"></div>';
						}
					?>
					<p class="tagline ">
						submitted <?php echo date("M j, Y g:i A", $r['created_utc']); ?> by <a href="https://old.reddit.com/user/<?php echo $r['author']; ?>" class="author may-blank"><?php echo $r['author']; ?></a>
						<?php
							// show author flair if present
							if(isset($r['author_flair_text']) && strlen($r['author_flair_text']) > 0) {
								echo '<span class="flair flair-' . $r['author_flair_css_class'] . '" title="' . $r['author_flair_text'] . '">' . $r['author_flair_text'] . '</span>';
							}
						 ?>
					</p>
					<ul class="flat-list buttons">
						<li class="first"><a href="/comments.php?id=<?php echo $r['id']; ?>" class="bylink comments may-blank" rel="nofollow"><?php echo $r['num_comments']; ?> comments</a></li>
						<li class="link-save-button save-button"><a href="#">save</a></li>
						<li class="report-button"><a href="#" class="reportbtn">report</a></li>
					</ul>
				</div>
				<!--<div class="expando expando-uninitialized" style="display: none">-->
				<div class="expando TDAhidden">
					<?php
						if($r['is_self'] == 1 && strlen($r['selftext']) > 0) {
							?>
								<div class="usertext-body may-blank-within md-container">
									<div class="md">
										<?php echo mdhtml($r['selftext']); ?>
									</div>
								</div>
							<?php
						} else if ($isImg == true) {
							?>
								<div class="media-preview">
									<div class="media-preview-content"> 
										<a href="<?php echo $url; ?>" class="may-blank"> 
											<img class="preview" src="<?php echo $url; ?>"> 
										</a> 
									</div>
								</div>
							<?php
						} else if($isYT == true) {
							echo ytEmbed($youtube_id);
						}
					?>
				</div>
			</div>
		</div>
	<?php
}

// next page
//echo '<a href="' . "$_SERVER[REQUEST_URI]" . '&offset=' . ((int)$offset + $pp) . '">Next</a>';
echo '<span class="nextprev">view more: <span class="next-button">';
$npUrl = modify_url_query((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]:$_SERVER[SERVER_PORT]$_SERVER[REQUEST_URI]", array('offset' => ((int)$offset + $pp)));
echo '<a href="' . $npUrl . '" rel="nofollow next">next ›</a>';
echo '</span></span>';


if($debug == true) {
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo 'Page generated in '.$total_time.' seconds. <br>';
	echo 'URL: ' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] <br>";
	echo 'URL: ' . "$_SERVER[REQUEST_URI]";
}

require("footer.php");
?>
</body>
</html>
