<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "../vars.php";
require_once "imageproc-common.php";

$id = $_GET['id'];
$ext = $_GET['ext'];


$zipFile = $dataRoot . "misc-images.zip";
$imgDir = $dataRoot . "images2/";

if(isset($useZipMisc)){$useZip = true;}


if($ext == "jpg" || $ext == "jpeg") {
	header('Content-Type: image/jpg');
} else if ($ext == "png") {
	header('Content-Type: image/png');
} else if ($ext == "gif" || $ext == "gifv") {
	header('Content-Type: image/gif');
} else if ($ext == "bmp") {
	header('Content-Type: image/bmp');
}

header('Content-Disposition: filename="' . $id . '.' . $ext . '"');

if ($useZip == true){
	zipImage($zipFile, "images2/" . $id . "." . $ext);
} else {
	readfile($imgDir . $_GET['id'] . "." . $ext);
}
?>