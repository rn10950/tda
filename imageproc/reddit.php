<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "../vars.php";
require_once "imageproc-common.php";

$id = $_GET['id'];
$ext = $_GET['ext'];

$imgDir = $dataRoot . "images/";
$zipFile = $dataRoot . "reddit-images.zip";

if(isset($useZipReddit)){$useZip = true;}

if($ext == "jpg") {
	header('Content-Type: image/jpg');
} else if ($ext == "png") {
	header('Content-Type: image/png');
} else if ($ext == "gif") {
	header('Content-Type: image/gif');
}

header('Content-Disposition: filename="' . $id . '.' . $ext . '"');

if ($useZip == true){
	zipImage($zipFile, "images/" . $id . "." . $ext);
} else {
	readfile($imgDir . $_GET['id'] . "." . $ext);
}
?>