<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "../vars.php";
require_once "imageproc-common.php";

$zipFile = $dataRoot . "thumbnails.zip";
$thumbDir = $dataRoot . "thumbnails/";


header('Content-Type: image/jpg');

if ($useZipThumbs == true){
	zipImage($zipFile, "thumbnails/" . $id . ".jpg");
} else {
	readfile($thumbDir . $_GET['id'] . ".jpg");
}
?>