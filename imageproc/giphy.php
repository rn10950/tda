<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "../vars.php";
require_once "imageproc-common.php";

$id = $_GET['id'];
$ext = "gif";

$zipFile = $dataRoot . "giphy.zip";


if(isset($useZipGiphy)){$useZip = true;}



$imgDir = $dataRoot . "giphy/";

header('Content-Type: image/gif');

header('Content-Disposition: filename="' . $id . '.' . $ext . '"');

if ($useZip == true){
	zipImage($zipFile, "giphy/" . $id . "." . $ext);
} else {
	readfile($imgDir . $_GET['id'] . "." . $ext);
}
?>