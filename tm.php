<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019, 2020 rn10950

$hTime = true;
require_once "functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Time Machine - The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="/tda.css">
		<script src="jquery.min.js"></script>
		<script src="jquery.datetimepicker.js"></script>
		<script>
		$( document ).ready(function(){
			$( "#datetimepicker" ).datetimepicker({
				format: 'Y/m/d H:i',
				inline: true,
				minDate: '2015/06/27',
				maxDate: '2020/06/29',
				defaultDate: new Date('November 8, 2016 21:00:00'),
				defaultTime: '21:00',
				yearStart: '2015',
				yearEnd: '2020',
				
			});
		});
		</script>
	</head>
	<body>
		<?php require 'header.php'; ?>
		<div class="tda-body">
			<h1>Time Machine</h1>
			<p>
				Pick a date and time to set the The_Donald Time Machine. All times eastern.
			</p><br>
			<form action="results.php" method="get" class="tda-body">
				<table class="tda-body">
					<tr>
						<td class="tda-body">
							<input type="radio" name="tm" value="0" checked>
						</td>
						<td class="tda-body">
							<input type="text" id="datetimepicker" name="dateTimeCal" value="2016/11/08 21:00">
						</td>
					</tr>
					<tr>
						<td class="tda-body">
							<input type="radio" name="tm" value="1">
						</td>
						<td class="tda-body">
							<label>Date (MM/DD/YYYY): </label><input type="text" name="dateTime">
						</td>
					</tr>
					<tr>
						<td class="tda-body">
							<input type="radio" name="tm" value="2">
						</td>
						<td class="tda-body">
							<label>Epoch Timestamp: </label><input type="text" name="dateTimeEpoch">
						</td>
					</tr>
				</table>
				
				<input type="submit" value="Go">
			</form>
		</div>
	</body>
</html>