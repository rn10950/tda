<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

//http://localhost/results.php?q=trump&sort=score&sortDir=DESC&type=all
if(isset($_GET['q'])) {
	$q = $_GET['q'];
	$qArr = array(
		"q" => $q,
		"sort" => "score",
		"sortDir" => "DESC",
		"type" => "all"
	);
	$url = "/results.php?" . http_build_query($qArr);
	header("Location: $url");
} else {
	echo "<h1>ERROR: No Query</h1>";
}
?>

