<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "vars.php";

function urlProc($url, $dom) {
	// enable global vars
	global $useArchiveLinks;
	global $dataRoot;
	
	// local vars
	$archiveUrl = "http://archive.today/?run=1&url=";
	
	// image processor files
	$imgurProc = "/imageproc/imgur.php";
	$redditProc = "/imageproc/reddit.php";
	$ruProc = "/imageproc/reddituploads.php";
	$slimgProc = "/imageproc/slimg.php";
	$magaProc = "/imageproc/magaimg.php";
	$miscProc = "/imageproc/misc.php";
	$giphyProc = "/imageproc/giphy.php";
	$twimgProc = "/imageproc/twimg.php";
	
	// set file paths	
	$slDatabase = $dataRoot . "slimg.db";
	$imgurDatabase = $dataRoot . "imgur.db";
	$miscDatabase = $dataRoot . "misc-images.db";
	$magaDatabase = $dataRoot . "magaimg.db";
	
	// sli.mg
	if($dom == "i.sli.mg") {
		preg_match('~/\K\w+(?=[^/]*$)~m', $url, $id);
		
		$q = 'SELECT ext FROM images WHERE "id" LIKE "' . SQLite3::escapeString($id[0]) . '"';
		
		// establish database connection
		$db = new SQLite3($slDatabase);
		$results = $db->query($q);
		while($r = $results->fetchArray()) {
			$extA = $r['ext'];
			break;
		}
		if(!isset($extA)) {
			return "ERROR";
		}
		$ext = explode("-", $extA)[0];
		#return $slRoot . $id[0] . "." . $ext;
		return $slimgProc . "?id=" . $id[0] . "&ext=" . $ext;
	}
	
	// i.redd.it
	else if($dom == "i.redd.it") {
		$img = substr(strrchr($url, "/"), 1);
		$rAr = explode(".", $img);
		$ext = $rAr[1];
		
		// hardcoded top images that were deleted by /u/spez on inauguration eve
		if($rAr[0] == "2ewfo07lnpay" || $rAr[0] == "mhbxrd2ropay" || $rAr[0] == "fm3eywjhopay") {
			$rAr[0] = "wfy54vbjlpay";
		}
		
		//return $reddRoot . $img;
		return $redditProc . "?id=" . $rAr[0] . "&ext=" . $ext;
	}
	
	
	// imgur
	else if($dom == "i.imgur.com" || $dom == "imgur.com") {
		preg_match('~/\K\w+(?=[^/]*$)~m', $url, $id);
			
		$q = 'SELECT ext FROM imgur WHERE "id" LIKE "' . SQLite3::escapeString($id[0]) . '"';
		
		// establish database connection
		$db = new SQLite3($imgurDatabase);
		$results = $db->query($q);
		while($r = $results->fetchArray()) {
			$extA = $r['ext'];
			break;
		}
		if(!isset($extA)) {
			return "ERROR";
		}
		$ext = explode("-", $extA)[0];
		return $imgurProc . "?id=" . $id[0] . "&ext=" . $ext;
		#return $imgurRoot . $id[0] . "." . $ext;	
	}
	
	// magaimg
	else if($dom == "i.magaimg.net" || $dom == "magaimg.net") {
		//preg_match('~/\K\w+(?=[^/]*$)~m', $url, $id);
		$urlA = explode("/", $url);
		$mAr = explode(".", $urlA[4]);
		$id = $mAr[0];
		$ext = $mAr[1];
		
		/*echo '<h1>' . $id . '</h1>';
		
		$q = 'SELECT ext FROM magaimg WHERE "id" LIKE "' . SQLite3::escapeString($id) . '"';
		
		// establish database connection
		$db = new SQLite3($magaDatabase);
		$results = $db->query($q);
		while($r = $results->fetchArray()) {
			$extA = $r['ext'];
			break;
		}
		if(!isset($extA)) {
			return $url;
		}
		$ext = explode("-", $extA)[0];
		return $magaRoot . $id . "." . $ext;*/	
		#return $magaRoot . $urlA[4];	
		return $magaProc . "?id=" . $id . "&ext=" . $ext;
	}
	
	// i.reddituploads.com
	else if($dom == "i.reddituploads.com") {
		$id = explode("/", explode('?', $url)[0])[3];
		
		// more hardcoded petty spez deletions
		if($id == "5942b91163f94d3bb57107c332ead519") {
			return '/imageproc/reddit.php?id=wfy54vbjlpay&ext=jpg';
		}
		
		return $ruProc . "?id=" . $id . "&ext=jpg";		
	}
	
	// giphy
	else if($dom == "giphy.com") {
		$b = explode('/', $url);
		$c = end($b);
		$d = explode("-", $c);
		$e = end($d);
		$id = explode('?', $e)[0];
		
		if($id == "fullscreen" || $id == "html5" || $id == "tile") {
			$t = explode('/', $url);
			$f = explode('-', $t[count($t)-2]);
			$g = end($f);
			$id = explode('?', $g)[0];
		}
		
		return $giphyProc . "?id=" . $id;		
	}
	
	// twitter images
	else if($dom == "pbs.twimg.com") {
		$b = explode('/', $url);
		$c = end($b);
		$id = explode('?', explode('.', $c)[0])[0];
		
		if(strpos($url, '.jpg') !== false || strpos($url, 'format=jpg') !== false) {
			$ext = 'jpg';
		} else if(strpos($url, '.png') !== false || strpos($url, 'format=png') !== false) {
			$ext = 'png';
		} else if(strpos($url, '.gif') !== false || strpos($url, 'format=gif') !== false) {
			$ext = 'gif';
		} else {
			$ext = 'jpg';
		}
		
		return $twimgProc . "?id=" . $id . "&ext=" . $ext;		
	}
	
	// other image urls
	else if(endsWith($url, ".jpg") || endsWith($url, ".jpeg") || endsWith($url, ".gif") || endsWith($url, ".gifv") || endsWith($url, ".png") || endsWith($url, ".bmp") ) {
		$q = 'SELECT id, ext FROM images WHERE "url" LIKE "' . SQLite3::escapeString($url) . '"';		
		// establish database connection
		$db = new SQLite3($miscDatabase);
		$results = $db->query($q);
		
		while($r = $results->fetchArray()) {
			$extA = $r['ext'];
			$idA = $r['id'];
			break;
		}
		
		// return raw url if not in database
		if(!isset($idA)) {
			return $url;
		}
		
		#return $miscRoot . $idA . '.' . $extA;	
		return $miscProc . "?id=" . $idA . "&ext=" . $extA;
	}
	
	
	if($useArchiveLinks == true) {
		return $archiveUrl . urlencode($url);
	}
	
	return $url;
}

?>