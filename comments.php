<?php
// r/The_Donald archive web frontend
// Copyright (c) 2019-2020 rn10950

require_once "functions.php";
require_once "krumo/class.krumo.php";

//$isCommentsPage == true;
$listPage = true;

$thread_id = $_GET['id'];

require 'comment-section.php';
$r = $link;
$thDate = $link['created_utc'];

$url = urlProc($r['url'], $r['domain']);
	
//$thumb = "/pepe.jpg";

//thumbnail
$thumbU = $r['thumbnail'];
if($thumbU == "default") {
	$thumb = "/pepe.jpg";
} elseif ($thumbU == "self") {
	$thumb = "/pepe.jpg";
} else {
	$thumbId = get_string_between($thumbU, ".com/", ".jp");
	$thumb = '/imageproc/thumb.php?id=' . $thumbId;
}

// youtube
if($r['domain'] == 'youtube.com' || $r['domain'] == 'youtu.be') {
	preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
	$youtube_id = $match[1];
	$isYT = true;
	//$thumb = "/youtube.jpg";
	//$thumb = "http://img.youtube.com/vi/" . $youtube_id . "/sddefault.jpg";
} else { $isYT = false; }

// twitter
if($r['domain'] == 'twitter.com') {
	//$thumb = "/twitter.png";
}

// images and thumbnails
if(endsWith($url, ".jpg") || endsWith($url, ".jpeg") || endsWith($url, ".gif") || endsWith($url, ".gifv") || endsWith($url, ".png") || endsWith($url, ".bmp") || strpos($url, "/imageproc/") !== false) {
	$isImg = true;
	//$thumb = $url;
} else {
	$isImg = false;
}

require "settingsLoader.php";

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $r['title']; ?> - The_Donald Archive</title>
		<link rel="stylesheet" type="text/css" href="/reddit.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $themeCSS; ?>">
		<link rel="stylesheet" type="text/css" href="/tda.css">
		<style>
			
			.TDAhidden {
				display: none !important;
			}
			.link-cursor {
				cursor: pointer;
			}
		</style>
		<script src="/jquery.min.js"></script>
		<script src="/comments.js"></script>
	</head>
	<body>
		<?php require 'header.php'; ?>
			<div class=" thing link self">
				<div class="midcol unvoted">
					<div class="arrow up"></div>
					<div class="score unvoted" title="<?php echo $r['score']; ?>"><?php echo $r['score']; ?></div>
					<div class="arrow down"></div>
				</div>
				<?php
				// thumbnail
				if($r['is_self'] == 1) {
					echo '<a class="thumbnail invisible-when-pinned self may-blank "></a>';
				} else {
					echo '<a class="thumbnail invisible-when-pinned may-blank "><img src="' . $thumb . '" alt="" width="70" height="70"></a>';
				}
				?>
				
				<div class="entry unvoted">
					<div class="top-matter">
						<p class="title"><a class="title may-blank " href="<?php echo $url; ?>" tabindex="1"><?php echo $r['title']; ?></a>
						<?php
							// show link flair if present
							if(isset($r['link_flair_css_text']) && strlen($r['link_flair_css_text']) > 0) {
								echo '<span class="linkflairlabel ' . $r['link_flair_css_class'] . '" title="' . $r['link_flair_css_text'] . '">' . $r['link_flair_css_text'] . '</span>';
							}
						?>					
						<span class="domain">(<a href="#SEARCH BY DOMAIN"><?php echo $r['domain']; ?></a>)</span></p>
						<p class="tagline ">
							submitted <?php echo date("M j, Y g:i A", $r['created_utc']); ?> by <a href="https://old.reddit.com/user/<?php echo $r['author']; ?>" class="author may-blank"><?php echo $r['author']; ?></a>
							<?php
								// show author flair if present
								if(isset($r['author_flair_text']) && strlen($r['author_flair_text']) > 0) {
									echo '<span class="flair flair-' . $r['author_flair_css_class'] . '" title="' . $r['author_flair_text'] . '">' . $r['author_flair_text'] . '</span>';
								}
							 ?>
						</p>
						<ul class="flat-list buttons">
							<li class="first"><a href="/comments.php?id=<?php echo $r['id']; ?>" class="bylink comments may-blank" rel="nofollow"><?php echo $r['num_comments']; ?> comments</a></li>
							<li class="link-save-button save-button"><a href="#">save</a></li>
							<li class="report-button"><a href="#" class="reportbtn">report</a></li>
						</ul>
					</div>
					<!--<div class="expando expando-uninitialized" style="display: none">-->
					<div class="expando">
						<?php
							if($r['is_self'] == 1 && strlen($r['selftext']) > 0) {
								?>
									<div class="usertext-body may-blank-within md-container">
										<div class="md">
											<?php echo mdhtml($r['selftext']); ?>
										</div>
									</div>
								<?php
							} else if ($isImg == true) {
								?>
									<div class="media-preview">
										<div class="media-preview-content"> 
											<a href="<?php echo $url; ?>" class="may-blank"> 
												<img class="preview" src="<?php echo $url; ?>"> 
											</a> 
										</div>
									</div>
								<?php
							} else if($isYT == true) {
								echo ytEmbed($youtube_id);
							}
						?>
					</div>
				</div>

				<div class="child"></div>
				<div class="clearleft"></div>
			</div>
		</div></div>
		<div class="commentarea">
			<div class="panestack-title"><span class="title">all <?php echo number_format($r['num_comments']); ?> comments</span></div>
			<?php formatComments($comments, 0); ?>
		</div>
		<?php //require 'footer.php'; ?>
	</body>
</html>